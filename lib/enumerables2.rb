#F -
require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr == []
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all?{|str| str.include?(substring)}
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
#select, reject, all none, any?
def non_unique_letters(string)
  arr = string.delete(" ").chars.uniq #str.delete return the new str, arr. delete returns the deleted item.
  arr.select{|ch| string.count(ch)>1}.sort
end
# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  longest = string.split(' ').sort_by {|word| word.length} [-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ("a".."z").reject{|ch| string.include?(ch)}
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select{|yr| not_repeat_year?(yr)}
end

def not_repeat_year?(year)
  arr = year.to_s.chars
  arr == arr.uniq
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders."
def one_week_wonders(songs)
    #select the songs that are not repeats.
    songs.select{|s| no_repeats?(s, songs)}.uniq
end

def no_repeats?(song_name, songs)
  #are there no elements such that the target song doesn't repeat in a row?
    (0..songs.length-1).none?{|idx| songs[idx] == song_name && songs[idx+1] == song_name}
end


# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distace and remove_punctuation.

def for_cs_sake(string)
  c_words = string.delete("?.,!").split(" ")
  c_words = c_words.select{|word| word.downcase.include?('c')}
  c_words.sort_by{|el| el.reverse.index('c')}[0]
end

def c_distance(word)
  #count the distance from the end to the first occurance of c
  word.reverse.downcase.index('c')
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]


def repeated_number_ranges(arr)
  indices = []
  var_start = nil

  arr.each_with_index do |el, idx|
    next_el = arr[idx+1] #become a nil value at the end
    if next_el == el
      var_start = idx unless var_start
    elsif var_start
      indices << [var_start, idx]
      var_start = nil
    end

  end

  indices#.select{|el| el[1]-el[0]>0}
end

#you can do 5 == nil, but you CANNOT do arr[i] == arr[i+1] where i+1 = nil
